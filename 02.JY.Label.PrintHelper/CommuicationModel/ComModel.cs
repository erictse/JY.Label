﻿using System.IO.Ports;
using System.Text;

namespace JY.Label.PrintHelper.CommuicationModel
{
    /// <summary>
    /// 串口模型
    /// </summary>
    public class ComModel : ICommuicationModel
    {
        #region # 构造器
        /// <summary>
        /// 构造器
        /// </summary>
        /// <param name="printerEncoding">打印机编码</param>
        /// <param name="portName">串口号</param>
        /// <param name="baudRate">波特率</param>
        /// <param name="parity">校验位</param>
        /// <param name="dataBits">数据位</param>
        /// <param name="stopBits">停止位</param>
        public ComModel(Encoding printerEncoding, string portName, int baudRate = 9600, Parity parity = Parity.None, int dataBits = 8, StopBits stopBits = StopBits.One)
        {
            this.PrinterEncoding = printerEncoding;
            this.PortName = portName;
            this.BaudRate = baudRate;
            this.Parity = parity;
            this.DataBits = dataBits;
            this.StopBits = stopBits;
        } 
        #endregion

        #region 打印机编码——Encoding PrinterEncoding
        /// <summary>
        /// 打印机编码
        /// </summary>
        public Encoding PrinterEncoding { get; set; }
        #endregion

        #region 串口号——string PortName
        /// <summary>
        /// 串口号
        /// </summary>
        public string PortName { get; set; }
        #endregion

        #region 波特率——int BaudRate
        /// <summary>
        /// 波特率
        /// </summary>
        public int BaudRate { get; set; }
        #endregion

        #region 校验位——Parity Parity
        /// <summary>
        /// 校验位
        /// </summary>
        public Parity Parity { get; set; }
        #endregion

        #region 数据位——int DataBits
        /// <summary>
        /// 数据位
        /// </summary>
        public int DataBits { get; set; }
        #endregion

        #region 停止位——StopBits StopBits
        /// <summary>
        /// 停止位
        /// </summary>
        public StopBits StopBits { get; set; } 
        #endregion

    }
}
