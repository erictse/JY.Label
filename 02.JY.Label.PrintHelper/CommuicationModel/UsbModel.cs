﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace JY.Label.PrintHelper.CommuicationModel
{
    /// <summary>
    /// Usb模型
    /// </summary>
    /// <remarks>
    /// USB打印打印机必须安装驱动。打印机名称【控制面板】→【设备打印机】→【打印机名称】
    /// </remarks>
    public class UsbModel : ICommuicationModel
    {
        #region # 构造器
        /// <summary>
        /// 构造器
        /// </summary>
        /// <param name="printerEncoding">打印机编码</param>
        /// <param name="printerName">打印机名称</param>
        public UsbModel(Encoding printerEncoding, string printerName)
        {
            this.PrinterEncoding = printerEncoding;
            this.PrinterName = printerName;
        }
        #endregion

        #region 打印机编码——Encoding PrinterEncoding
        /// <summary>
        /// 打印机编码
        /// </summary>
        public Encoding PrinterEncoding { get; set; }
        #endregion

        #region 打印机名称——string PrinterName

        /// <summary>
        /// 打印机名称
        /// </summary>
        /// <remarks>例ZDesigner ZT230-200dpi ZPL</remarks>
        public string PrinterName { get; set; }
        #endregion
    }
}
