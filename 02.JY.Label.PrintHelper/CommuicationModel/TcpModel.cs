﻿using System.Text;

namespace JY.Label.PrintHelper.CommuicationModel
{
    /// <summary>
    /// 网口模型
    /// </summary>
    public class TcpModel : ICommuicationModel
    {
        #region # 构造器
        /// <summary>
        /// 构造器
        /// </summary>
        /// <param name="printerEncoding">打印机编码</param>
        /// <param name="ipAddress">打印机IP地址</param>
        /// <param name="port">打印机端口号</param>
        public TcpModel(Encoding printerEncoding, string ipAddress, int port)
        {
            this.PrinterEncoding = printerEncoding;
            this.IpAddress = ipAddress;
            this.Port = port;
        }
        #endregion

        #region 打印机编码——Encoding PrinterEncoding
        /// <summary>
        /// 打印机编码
        /// </summary>
        public Encoding PrinterEncoding { get; set; }
        #endregion

        #region 打印机IP地址——string IpAddress
        /// <summary>
        /// 打印机IP地址
        /// </summary>
        public string IpAddress { get; set; }
        #endregion

        #region 打印机端口号——int Port

        /// <summary>
        /// 打印机端口号
        /// </summary>
        public int Port { get; set; }
        #endregion
    }
}
