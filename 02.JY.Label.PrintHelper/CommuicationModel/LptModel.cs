﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace JY.Label.PrintHelper.CommuicationModel
{
    /// <summary>
    /// Lpt模型
    /// </summary>
    public class LptModel : ICommuicationModel
    {
        #region # 构造器
        /// <summary>
        /// 构造器
        /// </summary>
        /// <param name="printerEncoding">打印机编码</param>
        /// <param name="lptPortName">打印机Lpt端口名称</param>
        public LptModel(Encoding printerEncoding, string lptPortName)
        {
            this.PrinterEncoding = printerEncoding;
            this.LptPortName = lptPortName;
        }
        #endregion

        #region 打印机编码——Encoding PrinterEncoding
        /// <summary>
        /// 打印机编码
        /// </summary>
        public Encoding PrinterEncoding { get; set; }
        #endregion

        #region 打印机Lpt端口号——string LptPortName

        /// <summary>
        /// 打印机Lpt端口名称
        /// </summary>
        /// <remarks>例Lpt1</remarks>
        public string LptPortName { get; set; }
        #endregion
    }
}
