﻿using System.Text;

namespace JY.Label.PrintHelper.CommuicationModel
{
    /// <summary>
    /// 通讯模型
    /// </summary>
    public interface ICommuicationModel
    {
        /// <summary>
        /// 打印机编码
        /// </summary>
        Encoding PrinterEncoding { get; set; }
    }
}
