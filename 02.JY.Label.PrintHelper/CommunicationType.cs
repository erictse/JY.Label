﻿namespace JY.Label.PrintHelper
{
    /// <summary>
    /// 打印机通讯方式
    /// </summary>
    public enum PrinterCommunicationType
    {
        Com,
        Tcp,
        Lpt,
        Usb
    }
}
