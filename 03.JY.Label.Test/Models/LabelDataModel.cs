﻿using Caliburn.Micro;

namespace JY.Label.Test.Models
{
    public class LabelDataModel : PropertyChangedBase
    {
        public LabelDataModel()
        {
            this.Param1 = "参数1";
            this.Param2 = "参数2";
            this.Param3 = "参数3";
            this.Param4 = "参数4";
            this.Param5 = "参数5";
            this.Param6 = "参数6";
            this.Param7 = "参数7";
            this.Param8 = "参数8";
            this.Param9 = "参数9";
            this.Param10 = "参数10";
            this.Param11 = "参数11";
            this.Param12 = "参数12";
        }
        #region Param1——string Param1
        private string _param1;

        public string Param1
        {
            get { return this._param1; }
            set { this._param1 = value; base.NotifyOfPropertyChange(); }
        }
        #endregion
        #region Param2——string Param2
        private string _param2;

        public string Param2
        {
            get { return this._param2; }
            set { this._param2 = value; base.NotifyOfPropertyChange(); }
        }
        #endregion
        #region Param3——string Param3
        private string _param3;

        public string Param3
        {
            get { return this._param3; }
            set { this._param3 = value; base.NotifyOfPropertyChange(); }
        }
        #endregion
        #region Param4——string Param4
        private string _param4;

        public string Param4
        {
            get { return this._param4; }
            set { this._param4 = value; base.NotifyOfPropertyChange(); }
        }
        #endregion
        #region Param5——string Param5
        private string _param5;

        public string Param5
        {
            get { return this._param5; }
            set { this._param5 = value; base.NotifyOfPropertyChange(); }
        }
        #endregion
        #region Param6——string Param6
        private string _param6;

        public string Param6
        {
            get { return this._param6; }
            set { this._param6 = value; base.NotifyOfPropertyChange(); }
        }
        #endregion
        #region Param7——string Param7
        private string _param7;

        public string Param7
        {
            get { return this._param7; }
            set { this._param7 = value; base.NotifyOfPropertyChange(); }
        }
        #endregion
        #region Param8——string Param8
        private string _param8;

        public string Param8
        {
            get { return this._param8; }
            set { this._param8 = value; base.NotifyOfPropertyChange(); }
        }
        #endregion
        #region Param9——string Param9
        private string _param9;

        public string Param9
        {
            get { return this._param9; }
            set { this._param9 = value; base.NotifyOfPropertyChange(); }
        }
        #endregion
        #region Param10——string Param10
        private string _param10;

        public string Param10
        {
            get { return this._param10; }
            set { this._param10 = value; base.NotifyOfPropertyChange(); }
        }
        #endregion
        #region Param11——string Param11
        private string _param11;

        public string Param11
        {
            get { return this._param11; }
            set { this._param11 = value; base.NotifyOfPropertyChange(); }
        }
        #endregion
        #region Param12——string Param12
        private string _param12;

        public string Param12
        {
            get { return this._param12; }
            set { this._param12 = value; base.NotifyOfPropertyChange(); }
        }
        #endregion
    }
}
