﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace JY.Label.Test.Models
{
    /// <summary>
    /// 标签大小单位
    /// </summary>
    public enum LabelSizeUnit
    {
        厘米,
        毫米,
        英寸
    }
}
