﻿using Caliburn.Micro;

namespace JY.Label.Test.Models
{
    /// <summary>
    /// 字典数据
    /// </summary>
    public class DicData : PropertyChangedBase
    {
        public DicData(string key,string value)
        {
            this.Key = key;
            this.Value = value;
        }
        private string _key;

        public string Key
        {
            get { return this._key; }
            set { this._key = value;base.NotifyOfPropertyChange(); }
        }
        private string _value;

        public string Value
        {
            get { return this._value; }
            set { this._value = value; base.NotifyOfPropertyChange(); }
        }
    }
}
