﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Data;

namespace JY.Label.Test.Converters
{
    /// <summary>
    /// bool转Visibility Converter
    /// </summary>
    public class BoolToVisibilityConverter : IValueConverter
    {
        /// <summary>
        /// 转换器
        /// </summary>
        /// <param name="value">Bool类型</param>
        /// <param name="targetType">类型</param>
        /// <param name="parameter">参数</param>
        /// <param name="culture">语言</param>
        /// <returns>parameter 为0时 true To Visible  为1时反过来</returns>
        public object Convert(object value, Type targetType, object parameter, System.Globalization.CultureInfo culture)
        {
            var vis = Visibility.Visible;
            if (parameter != null && value != null)
            {
                var val = bool.Parse(value.ToString());
                if (int.Parse(parameter.ToString()) == 0)
                {
                    vis = val ? Visibility.Visible : Visibility.Collapsed;
                }
                else
                {
                    vis = val ? Visibility.Collapsed : Visibility.Visible;
                }
            }
            else if (value != null && (bool)value)
            {
                vis = Visibility.Collapsed;
            }

            return vis;
        }

        public object ConvertBack(object value, Type targetType, object parameter, System.Globalization.CultureInfo culture)
        {
            throw new NotImplementedException();
        }
    }
}
