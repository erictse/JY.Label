﻿using System.Diagnostics;
using System.Linq;
using System.Windows;
using System.Windows.Documents;
using JY.Label.Test.Models;
using JY.Label.Test.ViewModels;

namespace JY.Label.Test.Views
{
    /// <summary>
    /// MainWindow.xaml 的交互逻辑
    /// </summary>
    public partial class ShellView : Window
    {
        public ShellView()
        {
            this.InitializeComponent();
        }

        private void Hyperlink_OnClick(object sender, RoutedEventArgs e)
        {
            Hyperlink link = sender as Hyperlink;
            Process.Start(new ProcessStartInfo(link.NavigateUri.AbsoluteUri));
        }
    }
}
